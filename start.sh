#!/bin/bash

cd docker
docker-compose up -d

cd ..

mvn clean package -DskipTests
java -jar target/campaigns-0.0.1-SNAPSHOT.jar
