# Sobre o projeto
Expor o serviço de Campanha, seguindo as regras de CRUD (Create, Read,
Update e Delete).

## Premissas
Eu, como usuário, quero administrar os dados da Campanha e fornecer mecanismos (APIs) para
`INCLUIR`, `CONSULTAR`, `ATUALIZAR`, `DELETAR` as campanhas. Para tanto, os critérios de aceite dessa
história são:

As campanhas deverão ser cadastradas de forma que o serviço retorne essas campanhas
seguindo a estrutura abaixo:
 - Nome Da Campanha;
 - ID do Time do Coração;
 - Data de Vigência;

O Sistema não deverá retornar campanhas que estão com a data de vigência vencidas;

No cadastramento de uma nova campanha, deve-se verificar se já existe uma campanha
ativa para aquele período (vigência), caso exista uma campanha ou N campanhas associadas
naquele período, o sistema deverá somar um dia no término da vigência de cada campanha
já existente. Caso a data final da vigência seja igual a outra campanha, deverá ser acrescido
um dia a mais de forma que as campanhas não tenham a mesma data de término de
vigência.Por fim, efetuar o cadastramento da nova campanha:

Exemplo:
```
Campanha 1 : inicio dia 01/10/2017 a 03/10/2017;
Campanha 2: inicio dia 01/10/2017 a 02/10/2017;
Cadastrando Campanha 3: inicio 01/10/2017 a 03/10/2017;

-> Sistema:
    Campanha 2 : 01/10/2017 a 03/10/2017 (porém a data bate com a campanha 1 e a 3, somando mais 1 dia)
        Campanha 2 : 01/10/2017 a 04/10/2017
    Campanha 1: 01/10/2017 a 04/10/2017 (bate com a data da campanha 2, somando mais 1 dia)
        Campanha 1: 01/10/2017 a 05/10/2017
    Incluindo campanha 3 : 01/10/2017 a 03/10/2017
```

As campanhas deveram ser controladas por um ID único;

No caso de uma nas campanhas já existentes, o sistema deverá ser capaz de fornecer
recursos para avisar outros sistemas que houve alteração nas campanhas existentes;

Neste exercício, deve-se contemplar a API que faz a associação entre o Cliente e as
Campanhas. Essa API é utilizada pelo próximo exercício. O Candidato deve analisar a melhor
forma e quais os tipos de atributos que devem ser utilizados nessa associação.

## Resolução

Abaixo segue informações relativas a resolução do problema:

### Informações da aplicação
Porta: **8090**

Endpoint para as campanhas: **/api/v1/campaigns**

Documentação e teste: **/swagger-ui.html** (ex: http://localhost:8090/swagger-ui.html)

> Para documentar a API usei o Swagger, caso necessite mais informações: [swagger.io](http://swagger.io)

Para realizar a persistencia foi utilizado o **MongoDB**

### Dependência

Para compilar esta aplicação é necessário que tenha instalado o **campaigns-common**, disponível em: [https://bitbucket.org/lagesco/campaigns-common](https://bitbucket.org/lagesco/campaigns-common)

Para realizar a instalação no repositório local, abra o terminal, navegue até o diretório da aplicação (_campaigns-common_) e execute o comando abaixo:

```cmd
mvn clean install
```

### Executando os teste
Para executar os teste, abra o terminal, navegue até o diretório da aplicação e execute o comando abaixo:

```cmd
mvn test
```

### Executando a aplicação

Esta aplicação utiliza o docker e docker-compose, caso não o tenha acesse [aqui](https://docs.docker.com/install/).
Antes de executar o script, edite o arquivo docker/docker-compose.yml, edite KAFKA_ADVERTISED_HOST_NAME e coloque o ip de seu computador
, como por exemplo:

```
KAFKA_ADVERTISED_HOST_NAME: 192.168.2.240
```

Para executar a aplicação, abra o terminal, navegue até o diretório da aplicação e execute o comando abaixo:


```cmd
./start.sh
```

Depois de iniciado o server poderá acessa-lo em:
[http://localhost:8090/swagger-ui.html](http://localhost:8090/swagger-ui.html)

### Exemplos de utilização

- Para consultar as campanhas cadastras ativas:

Execute um `GET` em **http://localhost:8090/api/v1/campaigns/**.
Inicialmente nada será retornado, pois o serviço usa a data atual caso nenhuma seja informada.

> Caso queira consultar o que já vem cadastradas:
> `GET` **http://localhost:8090/api/v1/campaigns/?date=2017-10-02**

- Para cadastras:

Execute um `POST` em **http://localhost:8090/api/v1/campaigns/**.
Informando o `body`:
```json
{
  "name": "Campanha 9",
  "teamId": "TEAM-2",
  "endDate": "2018-03-05",
  "startDate": "2018-03-01"
}
```

Para para o docker basta executar:

```cmd
./stop.sh
```


### Tecnologias utilizadas

- Java 8
- Spring boot
- MongoDB
- Swagger
- JUnit/AssertJ/Mockito
- Spring Data MongoDB
- Spring Web MVC
- Kafka
- Spring Kafka