package com.dk.campaigns.controller;

import com.dk.campaigns.helper.CampaignGenerator;
import com.dk.campaigns.model.Campaign;
import com.dk.campaigns.service.CampaignService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author Caio Andrade (dk)
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CampaignControllerTest {
    public static final String ROOT_PATH = "/api/v1";

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @MockBean
    private CampaignService service;

    @Autowired
    private CampaignController controller;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();

        Mockito.when(service.findActives(any(LocalDate.class))).thenReturn(CampaignGenerator.getAllCampaigns());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.registerModules(new Jdk8Module(), new JavaTimeModule());
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getAllActives() throws Exception {
        this.mockMvc.perform(get( ROOT_PATH + "/campaigns/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    public void getAllActivesErrors() throws Exception {
        Mockito.when(service.findActives(any(LocalDate.class))).thenReturn(Collections.emptyList());

        this.mockMvc.perform(get( ROOT_PATH + "/campaigns/"))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getById() throws Exception {
        Campaign campaign = CampaignGenerator.getAllCampaigns().get(0);
        Mockito.when(service.findOne(any(String.class))).thenReturn(campaign);

        this.mockMvc.perform(get( ROOT_PATH + "/campaigns/a1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.name", equalTo(campaign.getName())));
    }

    @Test
    public void getByIdErrors() throws Exception {
        Mockito.when(service.findOne(any(String.class))).thenReturn(null);

        this.mockMvc.perform(get( ROOT_PATH + "/campaigns/a1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void save() throws Exception {
        Campaign campaign = CampaignGenerator.getCampaignToBeSaved();
        campaign.setId("2a");
        Mockito.when(service.save(any(Campaign.class))).thenReturn(campaign);

        this.mockMvc.perform(post( ROOT_PATH + "/campaigns/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(CampaignGenerator.getCampaignToBeSaved())) )
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.name", equalTo(campaign.getName())))
                .andExpect(jsonPath("$.teamId", equalTo(campaign.getTeamId())));
    }

    @Test
    public void saveErrors() throws Exception {
        Campaign c = CampaignGenerator.getCampaignToBeSaved();
        c.setName("");

        this.mockMvc.perform(post( ROOT_PATH + "/campaigns/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(c)))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}
