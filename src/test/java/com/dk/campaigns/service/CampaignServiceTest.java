package com.dk.campaigns.service;

import com.dk.campaigns.exception.EntityNotFoundException;
import com.dk.campaigns.helper.CampaignGenerator;
import com.dk.campaigns.model.Campaign;
import com.dk.campaigns.repository.CampaignRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.junit.Assert.assertEquals;

/**
 * @author Caio Andrade (dk)
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CampaignServiceTest {
    @Autowired
    private CampaignService service;

    @Autowired
    private CampaignRepository repository;


    @Before
    public void setUp() throws Exception {
        repository.deleteAll();
        repository.save(CampaignGenerator.getAllCampaigns());
    }
    @Test
    public void saveCorrect() {
        Campaign campaign = CampaignGenerator.getCampaignToBeSaved();

        Campaign saved = service.save(campaign);

        assertEquals(saved.getEndDate(), CampaignGenerator.getCampaignToBeSaved().getEndDate());

        assertThat(repository.findAll())
                .as("As camapanhas 1 e 2 devem estar atualizadas")
                .extracting("name", "teamId", "startDate", "endDate")
                .contains(tuple("Campanha 1", "TEAM-1", LocalDate.of(2017,10,1), LocalDate.of(2017,10,5)),
                        tuple("Campanha 2", "TEAM-2", LocalDate.of(2017,10,1), LocalDate.of(2017,10,4)),
                        tuple("Campanha 3", "TEAM-3", LocalDate.of(2017,10,1), LocalDate.of(2017,10,3)));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void saveError() {
        Campaign campaign = CampaignGenerator.getCampaignToBeSaved();
        LocalDate end = campaign.getEndDate();
        campaign.setEndDate(campaign.getStartDate());
        campaign.setStartDate(end);

        service.save(campaign);
    }

    @Test
    public void saveCorrectWithContinuousVerification() {
        repository.save(CampaignGenerator.getExtraCampaigns());

        Campaign campaign = CampaignGenerator.getCampaignToBeSaved();

        Campaign saved = service.save(campaign);

        assertEquals(saved.getEndDate(), CampaignGenerator.getCampaignToBeSaved().getEndDate());

        assertThat(repository.findAll())
                .as("As camapanhas 1,2,4 e 5 devem estar atualizadas")
                .extracting("name", "teamId", "startDate", "endDate")
                .contains(tuple("Campanha 1", "TEAM-1", LocalDate.of(2017,10,1), LocalDate.of(2017,10,5)),
                        tuple("Campanha 2", "TEAM-2", LocalDate.of(2017,10,1), LocalDate.of(2017,10,4)),
                        tuple("Campanha 3", "TEAM-3", LocalDate.of(2017,10,1), LocalDate.of(2017,10,3)),

                        tuple("Campanha 4", "TEAM-1", LocalDate.of(2017,10,1), LocalDate.of(2017,10,7)),
                        tuple("Campanha 5", "TEAM-2", LocalDate.of(2017,10,1), LocalDate.of(2017,10,8)));
    }

    @Test
    public void findByPeriod() {
        assertThat(service.findByPeriod(
                LocalDate.of(2017, 10, 1), LocalDate.of(2017, 10, 2)))
                .as("Deve retorna uma Campanha (Campanha 2) pois somente ela está no periodo de vigência")
                .hasSize(1);

        assertThat(service.findByPeriod(
                LocalDate.of(2017, 10, 1), LocalDate.of(2017, 10, 3)))
                .as("Deve retorna somente 2 campanhas")
                .hasSize(2);

        assertThat(service.findByPeriod(
                LocalDate.of(2017, 10, 1), LocalDate.of(2017, 10, 4)))
                .as("Deve retorna somente 2 campanhas")
                .hasSize(2);
    }

    @Test
    public void deleteOK() {
        Campaign campaign = repository.findAll().get(0);

        service.delete(campaign.getId());
        assertThat(repository.findAll())
                .as("Deve conter somente 1 camanha cadastrada")
                .hasSize(1);
    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteError() {
        service.delete("abc");
    }

    @Test
    public void updateOK() {
        Campaign campaign = repository.findAll().get(0);
        campaign.setName("Campanha X");

        service.update(campaign.getId(), campaign);

        assertThat(repository.findAll())
                .as("As camapanhas 1 estar atualizadas")
                .extracting("name", "teamId", "startDate", "endDate")
                .contains(tuple("Campanha X", "TEAM-1", LocalDate.of(2017,10,1), LocalDate.of(2017,10,3)),
                        tuple("Campanha 2", "TEAM-2", LocalDate.of(2017,10,1), LocalDate.of(2017,10,2)));
    }

    @Test
    public void findActives() {
        List<Campaign> actives = service.findActives(LocalDate.of(2017, 10, 2));

        assertThat(actives)
                .as("As camapanhas 1 estar atualizadas")
                .hasSize(2);

    }
}
