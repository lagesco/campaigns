package com.dk.campaigns.helper;

import com.dk.campaigns.model.Campaign;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * @author Caio Andrade (dk)
 */
public class CampaignGenerator {

    public static List<Campaign> getAllCampaigns() {
        return Arrays.asList(
                CampaignBuilder.create()
                        .withName("Campanha 1")
                        .withTeamId("TEAM-1")
                        .withStartDate(LocalDate.of(2017, 10, 1))
                        .withEndDate(LocalDate.of(2017, 10, 3))
                        .build()

                , CampaignBuilder.create()
                        .withName("Campanha 2")
                        .withTeamId("TEAM-2")
                        .withStartDate(LocalDate.of(2017, 10, 1))
                        .withEndDate(LocalDate.of(2017, 10, 2))
                        .build()
        );
    }

    public static List<Campaign> getExtraCampaigns() {
        return Arrays.asList(
                CampaignBuilder.create()
                        .withName("Campanha 4")
                        .withTeamId("TEAM-1")
                        .withStartDate(LocalDate.of(2017, 10, 1))
                        .withEndDate(LocalDate.of(2017, 10, 5))
                        .build()

                , CampaignBuilder.create()
                        .withName("Campanha 5")
                        .withTeamId("TEAM-2")
                        .withStartDate(LocalDate.of(2017, 10, 1))
                        .withEndDate(LocalDate.of(2017, 10, 6))
                        .build()
        );
    }

    public static Campaign getCampaignToBeSaved() {
        return CampaignBuilder.create()
                .withName("Campanha 3")
                .withTeamId("TEAM-3")
                .withStartDate(LocalDate.of(2017, 10, 1))
                .withEndDate(LocalDate.of(2017, 10, 3))
                .build();
    }

}
