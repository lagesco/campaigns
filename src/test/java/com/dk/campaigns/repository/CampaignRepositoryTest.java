package com.dk.campaigns.repository;

import com.dk.campaigns.helper.CampaignGenerator;
import com.dk.campaigns.model.Campaign;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Teste que valida se o CampaignRepository esta funcionando corretamente
 *
 * @author Caio Andrade (dk)
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CampaignRepositoryTest {

    @Autowired
    private CampaignRepository repository;

    @Before
    public void setUp() {
        repository.deleteAll();
        repository.save(CampaignGenerator.getAllCampaigns());
    }

    /**
     * Busca todas as campanhas ativas (não retorna campanhas com data de vigencia vencida)
     */
    @Test
    public void findActiveCampaigns() {
        List<Campaign> campaigns = repository.findAllActives(LocalDate.of(2017, 10, 2));
        assertEquals(campaigns.size(), 2);

        campaigns = repository.findAllActives(LocalDate.of(2017, 10, 3));
        assertEquals(campaigns.size(), 1);

        campaigns = repository.findAllActives(LocalDate.of(2017, 10, 7));
        assertEquals(campaigns.size(), 0);

        campaigns = repository.findAllActives(LocalDate.of(2017, 9, 20));
        assertEquals(campaigns.size(), 0);
    }

    /**
     * Busca todas as campanhas dentro de um determinado periodo
     */
    @Test
    public void findCampaignsInPeriod() {
        List<Campaign> campaigns = repository.findByPeriod(LocalDate.of(2017, 10, 1)
                , LocalDate.of(2017, 10, 2));
        System.out.println(campaigns);
        assertEquals(campaigns.size(), 1);

        campaigns = repository.findByPeriod(LocalDate.of(2017, 10, 1)
                , LocalDate.of(2017, 10, 3));
        System.out.println(campaigns);
        assertEquals(campaigns.size(), 2);
    }

    /**
     * Busca todas as campanhas fora de um determinado periodo
     */
    @Test
    public void findCampaignsOutPeriod() {
        List<Campaign> campaigns = repository.findByPeriod(LocalDate.of(2017, 10, 1)
                , LocalDate.of(2017, 10, 1));
        assertEquals(campaigns.size(), 0);

        campaigns = repository.findByPeriod(LocalDate.of(2017, 10, 2)
                , LocalDate.of(2017, 10, 4));
        assertEquals(campaigns.size(), 0);
    }
}
