package com.dk.campaigns.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Entidade representa os dados do documento MongoDB de dados da Campanha (Campaign)
 *
 * @author Caio Andrade (dk)
 */
@ApiModel(value = "Campanha")
@Document
public class Campaign implements Serializable {
    @ApiModelProperty(value = "Id", notes = "Id da Campanha", readOnly = true)
    @Id
    private String id;

    @ApiModelProperty(value = "Nome", notes = "Nome da Campanha", required = true)
    @Size(min=5, max=100, message="Nome tem capacidade de 5 a 100 caracteres.")
    @NotNull(message="Nome da campanha é obrigatória.")
    private String name;

    @ApiModelProperty(value = "Id time coração", notes = "Id do time do coração", required = true)
    @Indexed
    @NotNull(message="Id time do coração é obrigatório.")
    private String teamId;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @ApiModelProperty(value = "Inicio da vigência", required = true)
    @Indexed
    @NotNull(message="O inicio da vigência é obrigatório.")
    private LocalDate startDate;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @ApiModelProperty(value = "Fim da vigência", required = true)
    @Indexed
    @NotNull(message="O fim da vigência é obrigatório.")
    private LocalDate endDate;

    public Campaign() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Campaign)) return false;
        Campaign campaign = (Campaign) o;
        return Objects.equals(id, campaign.id) &&
                Objects.equals(name, campaign.name) &&
                Objects.equals(teamId, campaign.teamId) &&
                Objects.equals(startDate, campaign.startDate) &&
                Objects.equals(endDate, campaign.endDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, teamId, startDate, endDate);
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "name='" + name + '\'' +
                ", teamId='" + teamId + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
