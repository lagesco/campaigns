package com.dk.campaigns.hook;

import com.dk.campaigns.model.Campaign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * @author Caio Andrade (dk)
 */
public class CampaignNotify {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignNotify.class);

    @Value("${kafka.topic.notify}")
    private String topicName;

    @Value("${kafka.topic.notify-delete}")
    private String topicDeleteName;

    @Autowired
    private KafkaTemplate<String, Campaign> kafkaTemplate;

    public void doNotify(Campaign campaigns) {
        LOGGER.info("Sending payload='{}' to topic='{}'", campaigns, topicName);
        kafkaTemplate.send(topicName, campaigns);
    }

    public void doNotifyDeletion(Campaign campaigns) {
        LOGGER.info("Sending payload='{}' to topic='{}'", campaigns, topicDeleteName);
        kafkaTemplate.send(topicDeleteName, campaigns);
    }
}
