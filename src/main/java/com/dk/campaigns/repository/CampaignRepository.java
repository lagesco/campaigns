package com.dk.campaigns.repository;

import com.dk.campaigns.model.Campaign;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Repositório para realizar as operações de persistencia sob o documento Campanha
 *
 * @author Caio Andrade (dk)
 */
@Repository
public interface CampaignRepository extends MongoRepository<Campaign, String> {

    @Query("{ 'startDate' : { $lte: ?0 }, 'endDate' : { $gte: ?0 } }, $orderby: { endDate : 1 }")
    List<Campaign> findAllActives(LocalDate date);

    @Query("{ 'startDate' : { $gte: ?0 }, 'endDate' : { $lte: ?1 } }, $orderby: { endDate : 1 }")
    List<Campaign> findByPeriod(LocalDate start, LocalDate end);

    @Query("{ 'endDate' : { $gte: ?0 } }, $orderby: { endDate : 1 }")
    List<Campaign> findByEndDateAfter(LocalDate end);

    @Query("{ 'teamId': ?0, 'startDate' : { $lte: ?1 }, 'endDate' : { $gte: ?2 } }, $orderby: { endDate : 1 }")
    List<Campaign> findByPeriod(String teamId, LocalDate start, LocalDate end);

}
