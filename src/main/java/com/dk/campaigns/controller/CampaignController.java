package com.dk.campaigns.controller;

import com.dk.campaigns.exception.EntityNotFoundException;
import com.dk.campaigns.model.Campaign;
import com.dk.campaigns.service.CampaignService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/v1/campaigns/")
@Api(value = "Campanhas", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE,
        tags = {"Endpoints Campanhas"}, description = "API Rest")
public class CampaignController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignController.class);

    private CampaignService service;

    @Autowired
    private ErrorAttributes errorAttributes;

    public CampaignController(CampaignService service) {
        this.service = service;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Retorna as campanhas ativas",
            notes = "É utilizada a data atual do server",
            response = Campaign[].class,
            responseContainer = "List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success"),
        @ApiResponse(code = 204, message = "No Content - Nenhuma campanha ativa para o periodo")
    })
    public ResponseEntity<List<Campaign>> getActives(
                @ApiParam(required = false, name = "date", value = "Data a ser pesquisada", defaultValue = "2017-10-02")
                @RequestParam(name = "date", required = false)
                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                         Optional<LocalDate> date) {
        List<Campaign> campaigns;
        if (date.isPresent()) {
            campaigns = service.findActives(date.get());
        } else {
            campaigns = service.findActives(LocalDate.now());
        }

        if (CollectionUtils.isEmpty(campaigns)) {
            LOGGER.debug("Nenhuma campanha encontrada");

            return ResponseEntity.noContent().build();
        }

        LOGGER.debug("Número de campanhas encontradas: {}", campaigns.size());

        return ResponseEntity.ok(campaigns);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Busca campanha por id",
            notes = "Retorna qualquer campanha que tenha id",
            response = Campaign.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404 , message = "Not found - Nenhuma campanha encontrada com o id informado")
    })
    public ResponseEntity<Campaign> getById(
            @ApiParam(required = true, name = "id", value = "Id da Campanha")
            @PathVariable("id") String id) {
        Campaign campaign = service.findOne(id);
        if (campaign == null) {
            LOGGER.debug("Campanha encontrada id {}", id);
            throw new EntityNotFoundException("Campanha não encontrada.");
        }

        return ResponseEntity.ok(campaign);
    }

    @GetMapping(value = "/team/{teamId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Busca campanha por id",
            notes = "Retorna qualquer campanha que tenha id",
            response = Campaign.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 404 , message = "Not found - Nenhuma campanha encontrada com o id informado")
    })
    public ResponseEntity<List<Campaign>> getByTeamId(
            @ApiParam(required = true, name = "teamId", value = "Id do time")
            @PathVariable("teamId") String teamId) {
        List<Campaign> campaign = service.findByTeamId(teamId);
        if (campaign == null) {
            LOGGER.debug("Campanha encontrada teamId {}", teamId);
            throw new EntityNotFoundException("Campanha não encontrada.");
        }

        return ResponseEntity.ok(campaign);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Cria uma campanha", response = Campaign.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Success"),
            @ApiResponse(code = 400 , message = "Bad Request")
    })
    public ResponseEntity<Campaign> save(
            @ApiParam(required = true, name = "campanha", value = "Nova Campanha")
            @Valid @RequestBody Campaign campaign) {
        Campaign saved = service.save(campaign);

        LOGGER.debug("Campanha cadastrada com sucesso {}", saved);

//        URI location = ServletUriComponentsBuilder
//                .fromCurrentRequest().path("/{id}")
//                .buildAndExpand(saved.getId()).toUri();
//
//        return ResponseEntity.created(location).build();
        return ResponseEntity.status(HttpStatus.CREATED).body(saved);
    }

    @PutMapping(value="/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Atualiza uma campanha", response = Campaign.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400 , message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not found")
    })
    public ResponseEntity<Campaign> update(
            @ApiParam(required = true, name = "id", value = "Id da Campanha")
            @PathVariable String id,
            @ApiParam(required = true, name = "campanha", value = "Campanha Atualizada")
            @Valid @RequestBody Campaign campaign) {
        Campaign currentCampaign = service.update(id, campaign);

        if (currentCampaign == null) {
            LOGGER.debug("Campanha não encontrada");
            throw new EntityNotFoundException("Campanha não encontrada.");
        }

        LOGGER.debug("Campanha atualizada com sucesso {}", currentCampaign);

        return ResponseEntity.ok(currentCampaign);
    }

    @DeleteMapping(value="/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Atualiza uma campanha", response = Campaign.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Success"),
            @ApiResponse(code = 404, message = "Not found")
    })
    public void delete(
            @ApiParam(required = true, name = "id", value = "Id da Campanha")
            @PathVariable String id) {
        service.delete(id);

        LOGGER.debug("Campanha removida {}", id);
    }

    public void populateDB() {

    }

}
