package com.dk.campaigns.service;

import com.dk.campaigns.model.Campaign;

import java.time.LocalDate;
import java.util.List;

/**
 * Interface para serviço de Campaign
 *
 * @author Caio Andrade (dk)
 */
public interface CampaignService {

    Campaign save(Campaign campaign);

    Campaign update(String id, Campaign campaign);

    List<Campaign> findByPeriod(LocalDate start, LocalDate end);

    void delete(String id);

    Campaign findOne(String id);

    List<Campaign> findActives(LocalDate now);

    List<Campaign> findByTeamId(String teamId);

}
