package com.dk.campaigns.service.impl;

import com.dk.campaigns.exception.EntityNotFoundException;
import com.dk.campaigns.model.Campaign;
import com.dk.campaigns.repository.CampaignRepository;
import com.dk.campaigns.service.CampaignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

/**
 * Implementação do Serviço de campanha
 *
 * @author Caio Andrade (dk)
 */
@Service
public class CampaignServiceImpl implements CampaignService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignServiceImpl.class);

    @Autowired
    private CampaignRepository repository;

    public Campaign save(Campaign campaign, boolean biggerThan, boolean updateDates) {
        if (campaign.getStartDate().isAfter(campaign.getEndDate())) {
            LOGGER.debug("A data inicial deve ser menor que a data final.");
            throw new DataIntegrityViolationException("A data inicial deve ser menor que a data final.");
        }

        LOGGER.debug("Salvando a campanha {}", campaign);

        if (updateDates) {
            updateCampaignsDates(campaign, biggerThan);
        }

        return repository.save(campaign);
    }

    private void updateCampaignsDates(Campaign campaign, boolean biggerThan) {
        List<Campaign> campaigns;
        if (biggerThan) {
            campaigns = repository.findByEndDateAfter(campaign.getEndDate());
        } else {
            campaigns = repository.findByPeriod(campaign.getStartDate(), campaign.getEndDate());
        }

        LOGGER.debug("Campanhas retornadas {}", campaigns);

        LOGGER.debug("Campanhas retornadas {}", campaigns.stream().anyMatch(e -> e.getEndDate().equals(campaign.getEndDate())));

        //Optional<LocalDate> maxDateOpt = campaigns.stream().max(Comparator.comparing(Campaign::getEndDate)).map(Campaign::getEndDate);
        if (campaigns.stream().anyMatch(e -> e.getEndDate().equals(campaign.getEndDate()))) {
            // se for igual que o fim da vigencia informada
            campaigns.stream()
                    //already sorted
                    .sorted(Comparator.comparing(Campaign::getEndDate))
                    .forEach(c -> {
                        LOGGER.debug("Processando campanha {}", c);
                        verifyCampaign(campaigns, c);
                        LOGGER.debug("Processada campanha {}", c);
                    });

            Campaign lastCampaign = campaigns.stream().max(Comparator.comparing(Campaign::getEndDate)).get();

            LOGGER.debug("Atualizações de campanhas {}", campaigns);

            // check to other campaigns on the data base
            save(lastCampaign, true, true);

            campaigns.remove(lastCampaign);

            repository.save(campaigns);
        }
    }

    private void verifyCampaign(List<Campaign> campaigns, Campaign campaign) {
        LocalDate f = campaign.getEndDate().plusDays(1);
        campaign.setEndDate(f);

        if (campaigns.stream().filter(e -> !e.equals(campaign)).anyMatch(e -> e.getEndDate().equals(f))) {
            verifyCampaign(campaigns, campaign);
        }
    }

    @Override
    public Campaign save(Campaign campaign) {
        return save(campaign, false, true);
    }

    @Override
    public Campaign update(String id, Campaign campaign) {
        Campaign currentCampaign = findOne(id);

        if (currentCampaign == null) {
            throw new EntityNotFoundException("Campanha não encontrada.");
        }

        boolean updateDate = !campaign.getStartDate().equals(currentCampaign.getStartDate())
                || !campaign.getEndDate().equals(currentCampaign.getEndDate());

        currentCampaign.setName(campaign.getName());
        currentCampaign.setTeamId(campaign.getTeamId());
        currentCampaign.setStartDate(campaign.getStartDate());
        currentCampaign.setEndDate(campaign.getEndDate());
        return save(currentCampaign, false, updateDate);
    }

    @Override
    public List<Campaign> findByPeriod(LocalDate start, LocalDate end) {
        return repository.findByPeriod(start, end);
    }

    @Override
    public void delete(String id) {
        Campaign campaign = repository.findOne(id);
        if (campaign == null) {
            throw new EntityNotFoundException("Campanha não encontrada.");
        }
        repository.delete(campaign);
    }

    @Override
    public Campaign findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public List<Campaign> findActives(LocalDate now) {
        return repository.findAllActives(now);
    }

    @Override
    public List<Campaign> findByTeamId(String teamId) {
        return repository.findByPeriod(teamId, LocalDate.now(), LocalDate.now());
    }

}
