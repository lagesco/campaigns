package com.dk.campaigns.interceptor;

import com.dk.campaigns.helper.CampaignBuilder;
import com.dk.campaigns.hook.CampaignNotify;
import com.dk.campaigns.model.Campaign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.stereotype.Service;

/**
 * Interceptor para as transações de Campaign
 *
 * @author Caio Andrade (dk)
 */
@Profile("!noNotification")
@Service
public class MongodbInterceptor extends AbstractMongoEventListener<Campaign> {

    @Autowired
    private CampaignNotify campaignNotify;

    /**
     * Após atualização ou cadastro de uma campanha notifica no serviço de mensageria sua alteração.
     *
     * @param event
     */
    @Override
    public void onAfterSave(AfterSaveEvent<Campaign> event) {
        super.onAfterSave(event);
        campaignNotify.doNotify(event.getSource());
    }

    /**
     * Após delete de uma campanha notifica no serviço de mensageria sua alteração.
     *
     * @param event
     */
    @Override
    public void onAfterDelete(AfterDeleteEvent<Campaign> event) {
        super.onAfterDelete(event);
        campaignNotify.doNotifyDeletion(CampaignBuilder.create()
                        .withId((String) event.getSource().get("id"))
                        .build());
    }
}
