package com.dk.campaigns.configuration;

import com.dk.campaigns.annotation.EnableCommons;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Configuração do Swagger
 *
 * @author Caio Andrade (dk)
 */
@Configuration
@EnableSwagger2
@EnableCommons
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("campaign-api")
                .apiInfo(apiInfo())
                .select()
//                    .apis(RequestHandlerSelectors.any())
//                    .paths(PathSelectors.any())
                    .paths(regex("/api.*"))
                .build();
    }

    @Bean
    public UiConfiguration uiConfig() {
        return new UiConfiguration(null);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("API Campanhas")
                .description("API para gestão de campanhas")
                .version("1.0")
                .build();
    }
}