package com.dk.campaigns.configuration;

import com.dk.campaigns.hook.CampaignNotify;
import com.dk.campaigns.model.Campaign;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Configuração de notificação (Kafka)
 *
 * @author Caio Andrade (dk)
 */
@Profile("!noNotification")
@Configuration
public class NotificationConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationConfig.class);

    @Value("${kafka.server}")
    private String bootstrapServers;

    @Bean
    public Map<String, Object> producerConfigs() {
        LOGGER.debug("Kafa - using {} address.", bootstrapServers);
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        return props;
    }

    @Bean
    public ProducerFactory<String, Campaign> producerFactory() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }

    @Bean
    public KafkaTemplate<String, Campaign> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

    @Bean
    public CampaignNotify campaignNotify() {
        return new CampaignNotify();
    }
}
