package com.dk.campaigns.configuration;

import com.dk.campaigns.model.Campaign;
import com.dk.campaigns.repository.CampaignRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Inicializa a base de dados com os dados de data.json
 *
 * @author Caio Andrade (dk)
 */
@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartupApplicationListener.class);

    @Autowired
    private CampaignRepository repository;

    @Autowired
    private ObjectMapper mapper;

    @Value("classpath:data.json")
    private Resource res;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        LOGGER.debug("Applicaton started");
        PageRequest pageRequest = new PageRequest(0, 1);
        Page<Campaign> items = repository.findAll(pageRequest);

        if (items.getTotalElements() == 0) {
            LOGGER.debug("Initializing DB");
            try {
                Path path = Paths.get(res.getURI());
                String s = new String(Files.readAllBytes(path));

                List<Campaign> campaigns = mapper.readValue(s, new TypeReference<List<Campaign>>() { });
                repository.save(campaigns);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}