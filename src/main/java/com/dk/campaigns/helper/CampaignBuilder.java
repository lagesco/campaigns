package com.dk.campaigns.helper;

import com.dk.campaigns.model.Campaign;

import java.time.LocalDate;

/**
 * @author Caio Andrade (dk)
 */
public final class CampaignBuilder {
    private String id;
    private String name;
    private String teamId;
    private LocalDate startDate;
    private LocalDate endDate;

    private CampaignBuilder() {
    }

    public static CampaignBuilder create() {
        return new CampaignBuilder();
    }

    public CampaignBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public CampaignBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public CampaignBuilder withTeamId(String teamId) {
        this.teamId = teamId;
        return this;
    }

    public CampaignBuilder withStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public CampaignBuilder withEndDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public Campaign build() {
        Campaign campaign = new Campaign();
        campaign.setId(id);
        campaign.setName(name);
        campaign.setTeamId(teamId);
        campaign.setStartDate(startDate);
        campaign.setEndDate(endDate);
        return campaign;
    }
}
